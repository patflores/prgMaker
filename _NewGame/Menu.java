﻿package _NewGame;
/*




초기메뉴


 */
public class Menu {
	public static void mainMenu() {
		System.out.println("**********************");
		System.out.println("***Programmer Maker***");
		System.out.println("**********************");
		
		System.out.println("1. 시작");
		System.out.println("2. 로드");
		System.out.println("3. 종료");
	}
	
	public static void subMenu() {
		System.out.println();
		System.out.println();
	}
	
	public static void menuSel() {
		int sel =0;

		while (true) {
			Menu.mainMenu();
			System.out.print("Menu Number : ");
			
			try {
				sel=Integer.parseInt(ReadValue.input());	
			} catch (NumberFormatException e) {
				System.out.println("숫자만 입력해줘\n");
				sel = 35628908;
			} catch (Exception e) {}
			
			switch (sel) {
			case 1:
				_SetPrint.Titles("~~~Game Start~~~", 90);
				/*System.out.println("~~Game Start~~");
				System.out.println();*/
				Op_Story.story();
				break;
			case 2:
				System.out.println("Load System은 DB연동 후에 지원될꺼야~");
				break;
			case 3:
				System.out.println("종료할게~");
				System.exit(1);
			case 35628908:
				break;
			default:
				System.out.println("제대로 된 숫자를 입력해 줘");
				break;
			}
		}
		
		
		
	}
}
