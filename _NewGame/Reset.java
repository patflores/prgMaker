﻿package _NewGame;

public class Reset extends YourData {
	public void resetSet() {
		setName(null);
		setAge(0);
		setJender(null);
		setMoney(800); // 초기자금
		setThisDay(0); // 기본적으로 1일차
		setHtml(0);
		setJquery(0);
		setPhp(0);
		setAsp(0);
		setJava(0);
		setJsp(0);
		setCpp(0);
		setCshop(0);
		setPython(0);
		setObject_c(0);
		setSwift(0);
		setHappy(100);
		setItemFirstView(0);
	}

}
