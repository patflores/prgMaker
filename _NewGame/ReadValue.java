﻿package _NewGame;
/*




입력받기



 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadValue {
	public static String input() { //static은 메모리에 항상 있음, 누구든 접근하도록!
									//지역변수는 썼다가 사라지지만, static은 다름
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String str="";
		try {
			str = in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
		
	}
}
