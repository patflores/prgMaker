﻿package _NewGame;

public class ItemInfo extends YourData {
	
	private String itemName="";
	private int itemNumber;
	
	public ItemInfo() {super();}
	
	public ItemInfo(String itemName, int itemNumber) {
		this.itemName=itemName;
		this.itemNumber=itemNumber;
	}

		
	
	public String getItemName() {
		return itemName;
	}
	
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(int itemNumber) {
		this.itemNumber = itemNumber;
	}
	
	public void useMoney(int mMoney) {
		int money = getMoney();
		money -= mMoney;
		setMoney(money);	
	}
	
	public void i_daecul() {
		setItemName("채무용지");
		setItemNumber(12);
		int money = getMoney();
	}
	
	public void i_books() {
		setItemName("전문서적");
		setItemNumber(13);
		useMoney(900);
		
	}
	
	public void i_note() {
		setItemName("보급형 노트북");
		setItemNumber(14);
		useMoney(1400);
	}
	
	public void i_note2() {
		setItemName("최신형 노트북");
		setItemNumber(15);
		useMoney(2000);
	}
	
	public void i_apple() {
		setItemName("Apple 전자기기");
		setItemNumber(16);
		useMoney(3000);
	}
	
	public void i_helper() {
		setItemName("도우미 ");
		setItemNumber(17);
		useMoney(2300);
	}	
	public void i_aihelper() {
		setItemName("AI 로봇 도우미 ");
		setItemNumber(18);
		useMoney(8500);
	}		
	public void i_friends() {
		setItemName("사귀는 사람");
		setItemNumber(19);
	}		
	
	public void itemSellMent(int inum) {
		_SetPrint.MSG("이 물건은 " + inum + "원에 사겠습니다.", 600);
	}
	
	public void itemSellSystem(int inum) {
		int money = getMoney();
		money += inum;
		setMoney(money);
		_SetPrint.MSG("현재 보유 금액 : " + getMoney() + "원", 600);
	}
	
	public void noSell() {
		_SetPrint.MSG("해당 물건은 구매하지 않습니다.", 600);
	}
	
	public void itemDelChecker(int inum) {
		if (inum==13) {
			itemSellMent(450);
			itemSellSystem(450);
		} else if (inum==14) {
			itemSellMent(700);
			itemSellSystem(700);
		} else if (inum==15) {
			itemSellMent(1000);
			itemSellSystem(1000);
		} else if (inum==16) {
			itemSellMent(2000);
			itemSellSystem(2000);
		} else if (inum==17) {
			itemSellMent(1150);
			itemSellSystem(1150);
		} else if (inum==18) {
			itemSellMent(4250);
			itemSellSystem(4250);
		} else if (inum==19) {
			itemSellMent(600);
			itemSellSystem(600);
		}else if (inum==12)  {
			noSell();
		}
	}

	

	
	public String getInfo() {
		String str="["+getItemNumber()+"] - "+ getItemName();
		return str;
	}
	
	public String toString() {
		String str="["+getItemNumber()+"] - "+ getItemName();
		return str;
	}
}
