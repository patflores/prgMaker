package _NewGame;

import java.util.Random;

/*
 * 
 * 
 * 능력치 
 * 
 * 
 * 
 */
public class StudyLevel extends YourData {
	MaxChk mck = new MaxChk();
	AllMinus am = new AllMinus();
	_GUIApp gapp = new _GUIApp();
	Random randomGenerator = new Random();
	int startpoint = 0,endpoint = 0, subStart=0, subEnd=0;
	double rangepoint=0.0, subrangepoint=0.0;
	
	public int PointReturn_Minu() { //소폭감소
		subStart = 30;
		subEnd = 50;
		subrangepoint = subEnd - subStart + 1;
		int randomIntEasy = (int)(randomGenerator.nextDouble() * subrangepoint + subStart);
		return randomIntEasy;
	}
	
	public int PointReturn_Easy() { //쉬운용어
		int ageAvd = getAge();
		
		if  (ageAvd < 18) {
			subStart = 56;
			subEnd = 68;
		} else if (ageAvd < 22) {
			subStart = 48;
			subEnd = 56;
		} else if (ageAvd < 29) {
			subStart = 32;
			subEnd = 54;
		} else if (ageAvd < 35) {
			subStart = 28;
			subEnd = 48;
		} else if (ageAvd < 39) {
			subStart = 21;
			subEnd = 42;
		} else if (ageAvd < 49) {
			subStart = 17;
			subEnd = 29;
		} else {
			subStart = 16;
			subEnd = 23;
		}
		
		subrangepoint = subEnd - subStart + 1;
		int randomIntEasy = (int)(randomGenerator.nextDouble() * subrangepoint + subStart);
		return randomIntEasy;
	}
	public int PointReturn_Midium() { // 중급용어

		int ageAvd = getAge();
		
		if  (ageAvd < 18) {
			subStart = 35;
			subEnd = 45;	
		} else if (ageAvd < 22) {
			subStart = 29;
			subEnd = 42;
		} else if (ageAvd < 29) {
			subStart = 25;
			subEnd = 31;
		} else if (ageAvd < 35) {
			subStart = 19;
			subEnd = 29;
		} else if (ageAvd < 39) {
			subStart = 17;
			subEnd = 26;
		} else if (ageAvd < 49) {
			subStart = 14;
			subEnd = 20;
		} else {
			subStart = 8;
			subEnd = 14;
		}
		subrangepoint = subEnd - subStart + 1;
		int randomIntEasy = (int)(randomGenerator.nextDouble() * subrangepoint + subStart);
		return randomIntEasy;
	}	
	public int PointReturn_Hard() { // 고급용어
		int ageAvd = getAge();
		
		if  (ageAvd < 18) {
			subStart = 25;
			subEnd = 33;
		} else if (ageAvd < 22) {
			subStart = 18;
			subEnd = 27;
		} else if (ageAvd < 29) {
			subStart = 12;
			subEnd = 25;
		} else if (ageAvd < 35) {
			subStart = 11;
			subEnd = 23;
		} else if (ageAvd < 39) {
			subStart = 10;
			subEnd = 18;
		} else if (ageAvd < 49) {
			subStart = 8;
			subEnd = 13;
		} else {
			subStart = 4;
			subEnd = 6;
		}
		subrangepoint = subEnd - subStart + 1;
		int randomIntEasy = (int)(randomGenerator.nextDouble() * subrangepoint + subStart);
		return randomIntEasy;
	}	
		
	
	public void returnMenu() {
		SelectToday st = new SelectToday();
		st.menuPrint();
	}
	
	public void minusHappy(int point) {
		int happyPoint = getHappy();
		happyPoint -= point;
		setHappy(happyPoint);
		_SetPrint.MSG(getName() + "의 기분이 " + point + "만큼 감소 ( 현재 행복도 : " + getHappy() + ")", 600);
	}
	
	public void plusHappy(int point) {
		int happyPoint = getHappy();
		happyPoint += point;
		setHappy(happyPoint);
		mck.MaxChkList();
		_SetPrint.MSG(getName() + "의 기분이 " + point + "만큼 상승 ( 현재 행복도 : " + getHappy() + ")", 600);
	}
	
	
	public void PrintMsg(String subject, int points) { //과목배우기
		_SetPrint.MSG("", 600);
		//gapp.mentPrint(subject + "를 배우는중");
		_SetPrint.Studing(subject + "를 배우는중", 600);
		_SetPrint.MSGL(getName() + "의 " + subject + " 능력이 " + -points + "만큼 증가했습니다! ( 현재 " + subject + " 능력 : ", 0);
		//gapp.mentPrint(getName() + "의 " + subject + " 능력이 " + -points + "만큼 증가했습니다! ( 현재 " + subject + " 능력 : ");
		
	}
	
	public void PrintAlba(String subject, int points, int minuspoint) { //알바
		_SetPrint.MSG("", 600);
		_SetPrint.Studing(subject + "를 하는중", 600);
		_SetPrint.MSGL(getName() + "의 " + subject + " 능력이 " + -points + "만큼 증가했습니다! ( 현재 " + subject + " 능력 : ", 0);
		
		am.AllminusData(minuspoint); //3점감소
		mck.MaxChkList();
		
	}
		
	
	public void minusDay() { /// 횟수감소
		int todayminus = getWorkTime();
		todayminus-=1;
		setWorkTime(todayminus);
	}
	
	public void minusMoney(int minus_money) { // 돈감소
		int money = getMoney();
		money -= minus_money;
		setMoney(money);
		_SetPrint.MSG(minus_money + "원 사용! ( 현재 보유금액 : " + getMoney() + " )", 600);
	}
	
	public void MinusWeb() { // 비슷한용어 미숙하니 감소
		_SetPrint.MSG(getName() + "는 비슷한용어 (ASP-JSP-PHP) 중 하나가 미숙해서 헷갈려 합니다.",600);
		

		int mnsPoint = PointReturn_Minu(); 
		
		int phpPoint = getPhp();
		int aspPoint = getAsp();
		int jspPoint = getJsp();
		
		phpPoint -= mnsPoint;
		aspPoint -= mnsPoint;
		jspPoint -= mnsPoint;
		
		setPhp(phpPoint);
		setAsp(aspPoint);
		setJsp(jspPoint);
		
		
		_SetPrint.MSG("해당용어들의 능력치가 " +mnsPoint+ " 만큼 감소합니다.",600);
	}	
	
	public void MinusClang() { // 비슷한용어 미숙하니 감소
		_SetPrint.MSG(getName() + "는 애매하게 배운 C++때문에 Java를 헷갈려 합니다.",600);
		

		int mnsPoint = PointReturn_Minu(); 
		
		int cppPoint = getCpp();
		cppPoint -= mnsPoint;
		setJava(cppPoint);
		
		_SetPrint.MSG("C++ 능력치가  " +mnsPoint+ " 만큼 감소합니다.",600);
	}
	
	public void MinusCseris() { // 비슷한용어 미숙하니 감소
		_SetPrint.MSG(getName() + "는 비슷한용어 (C-C+-C#) 중 하나가 미숙해서 헷갈려 합니다.",600);
		

		int mnsPoint = PointReturn_Minu(); 
		
		int clangPoint = getClang();
		int cppPoint = getCpp();
		int cshopPoint = getCshop();
		
		clangPoint -= mnsPoint;
		cppPoint -= mnsPoint;
		cshopPoint -= mnsPoint;
		
		setClang(clangPoint);
		setCpp(cppPoint);
		setCshop(cshopPoint);
		
		_SetPrint.MSG("해당용어들의 능력치가 " +mnsPoint+ " 만큼 감소합니다.",600);
	}
	
	public void MinusApple() { // 비슷한용어 미숙하니 감소
		_SetPrint.MSG(getName() + "는 비슷한용어 (Objective-C or Swift) 중 하나가 미숙해서 헷갈려 합니다.",600);
		

		int mnsPoint = PointReturn_Minu(); 
		
		int swiPoint = getSwift();
		int obPoint = getObject_c();

		swiPoint -= mnsPoint;
		obPoint -= mnsPoint;

		
		setSwift(swiPoint);
		setObject_c(obPoint);

		_SetPrint.MSG("해당용어들의 능력치가 " +mnsPoint+ " 만큼 감소합니다.",600);
	}
	
	
	
	public void plusMoney(int plus_money) { // 돈증가
		int money = getMoney();
		money += plus_money;
		setMoney(money);
		_SetPrint.MSG(plus_money + "원 벌음! ( 현재 보유금액 : " + getMoney() + " )", 600);
	}	
	
	public void ifStudy(Object names) {
		
		
	}
	
	public void noMoney(String names) { //잔액부족
		_SetPrint.MSG("돈이 부족해서" + names + "를 배울 수 없어!", 600);
		
	}
	
	public void noStudy(String parentsProgram, String names) { //능력부족
		System.out.println("");
		_SetPrint.MSG(parentsProgram + " 능력치가 부족해서 " + names + "를 배울 수 없어!", 600);
		
	}	
	
	public void itemCheck() {
		//아이템체크기
	}
	
	public void html() {
		String subjectName = "HTML/CSS";

		if (getMoney() < getHtmlMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
			int level = getHtml();
			level += PointReturn_Easy(); // Easy
			
			int plusPoint = getHtml() - level;
			setHtml(level);
			
			PrintMsg(subjectName, plusPoint); //능력치 이름
			
			_SetPrint.MSG(getHtml() + ")", 600);
			plusHappy(5); //행복도 5증가
			minusMoney(getHtmlMoney()); // 돈 감소
			minusDay(); // 일정 횟수 감소
			//returnMenu(); // 메뉴로 돌아가지
		}
	}
	
	
	
	public void jquery() {
		
		String subjectName = "JavaScript/Jquery";
		if (getMoney() < getJqueryMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
			if (getHtml() >= 170) {
				int level = getJquery();
				level += PointReturn_Easy(); // Eassy
				
				int plusPoint = getJquery() - level;
				setJquery(level);
				
				itemCheck();
				if (getClang() > 200) {
					level = getJquery();
					level += 15; // 15점 증가
					setJquery(level);
					//_SetPrint.MSG("C언어 능력으로 해당 능력치 15점 추가!",600);
				}
					
				PrintMsg(subjectName, plusPoint); //능력치 이름
				_SetPrint.MSG(getJquery() + ")", 600);
				
				minusMoney(getJqueryMoney()); // 돈 감소
				plusHappy(5); //행복도 5증가
				
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
			} else {
				noStudy("HTML/CSS", subjectName);
				//returnMenu();
			}
		}
	}
	
	public void php() {
		
		String subjectName = "PHP";
		
		if (getMoney() < getPhpMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
			if (getHtml() >= 170 && getJquery() >= 140) {
				int level = getPhp();
				level += PointReturn_Midium(); // 15점 증가
				int plusPoint = getPhp() - level;
				setPhp(level);
				PrintMsg(subjectName, plusPoint); //능력치 이름
				_SetPrint.MSG(getPhp() + ")", 600);
				
				itemCheck();
				
				if (getAsp() >= 200 || getJsp() >= 200) {
					level = getPhp();
					level += 15; // 15점 증가
					setPhp(level);
					_SetPrint.MSG("ASP or JSP 능력으로 15점 추가!",600);
				} else if(getAsp() >= 30 || getJsp() >= 30) { //30점 이상이면서 120이하
					if(getAsp() <= 120 || getJsp() <= 120) {
						MinusWeb(); //비슷한 용어능력치 감소
					}
				}
				if (getClang() > 200) {
					level = getPhp();
					level += 15; // 15점 증가
					setPhp(level);
					_SetPrint.MSG("C언어 능력으로 해당 능력치 15점 추가!",600);
				}
					
				

				minusMoney(getPhpMoney()); // 돈 감소
				plusHappy(5); //행복도 5증가
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
			} else {
				noStudy("HTML/CSS 혹은 JavaScript/Jquery", subjectName);
				//returnMenu();
			}
		}
	}	
	
	public void asp() {
		
		String subjectName = "ASP";
		
		if (getMoney() < getAspMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
			if (getHtml() >= 170 && getJquery() >= 140) {
				int level = getAsp();
				level += PointReturn_Midium(); // 15점 증가
				int plusPoint = getAsp() - level;
				setAsp(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				_SetPrint.MSG(getAsp() + ")", 600);
				itemCheck();
				if (getPhp() >= 200 || getJsp() >= 200) {
					level = getAsp();
					level += 15; // 15점 증가
					setAsp(level);
					_SetPrint.MSG("PHP or JSP 능력으로 15점 추가!",600);
				}else if(getPhp() >= 30 || getJsp() >= 30) { //30점 이상이면서 120이하
					if(getPhp() <= 120 || getJsp() <= 120) {
						MinusWeb(); //비슷한 용어능력치 감소
					}
				}
				if (getClang() > 200) {
					level = getAsp();
					level += 15; // 15점 증가
					setAsp(level);
					_SetPrint.MSG("C언어 능력으로 해당 능력치 15점 추가!",600);
				}
				
	
				
				minusMoney(getAspMoney()); // 돈 감소
				plusHappy(5); //행복도 5증가
				
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
			} else {
				noStudy("HTML/CSS 혹은 JavaScript/Jquery", subjectName);
				//returnMenu();
			}
		}
	}		
	
	public void jsp() {
		
		String subjectName = "JSP";
		
		if (getMoney() < getJspMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
			if (getHtml() >= 170 && getJquery() >= 140 && getJava() >= 200) {
				int level = getJsp();
				level += PointReturn_Midium(); // 15점 증가
				int plusPoint = getJsp() - level;
				setJsp(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				_SetPrint.MSG(getJsp() + ")", 600);
				itemCheck();
				if (getPhp() >= 200 || getAsp() >= 200) {
					level = getJsp();
					level += 15; // 15점 증가
					setJsp(level);
					_SetPrint.MSG("PHP or ASP 능력으로 15점 추가!",600);
				} else if(getPhp() >= 30 || getAsp() >= 30) { //30점 이상이면서 120이하
					if(getPhp() <= 120 || getAsp() <= 120) {
						MinusWeb(); //비슷한 용어능력치 감소
					}
				}

				
				minusMoney(getJspMoney()); // 돈 감소
				plusHappy(5); //행복도 5증가
				
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
			} else {
				//noStudy("HTML/CSS 혹은 JavaScript/Jquery, JAVA", subjectName);
				//returnMenu();
			}
		}
	}	
	
	
	public void java() {
		
		String subjectName = "Java";
		
		if (getMoney() < getJavaMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
				int level = PointReturn_Hard();
				level += 15; // 15점 증가
				int plusPoint = getJava() - level;
				setJava(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				_SetPrint.MSG(getJava() + ")", 600);
				itemCheck();
				if (getCpp() >= 200 || getCshop() >= 200) {
					level = getJava();
					level += 20; // 15점 증가
					setJava(level);
					_SetPrint.MSG("C++ / C# 능력으로 20점 추가!",600);
				} else if(getCpp() >= 30) { //30점 이상이면서 120이하
					if(getCpp() <= 120) {
						MinusClang(); //c언어감소
					}
				}
				
				minusMoney(getJavaMoney()); // 돈 감소
				plusHappy(6); //행복도 5증가
				
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
		
		}
	}
	
	
	public void clnag() {
		
		String subjectName = "C언어";
		
		if (getMoney() < getClangMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
				int level = getClang();
				level += PointReturn_Hard(); // 15점 증가
				int plusPoint = getClang() - level;
				setClang(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				_SetPrint.MSG(getClang() + ")", 600);
				itemCheck();
				if (getCpp() >= 200 || getCshop() >= 200) {
					level = getClang();
					level += 30; // 15점 증가
					setClang(level);
					_SetPrint.MSG("C+ or C# 능력으로 30점 추가!",600);
				} else if(getCpp() >= 30 || getCshop() >= 30) { //30점 이상이면서 120이하
					if(getCpp() <= 120 || getCshop() <= 120) {
						MinusCseris(); //비슷한 용어능력치 감소
					}
				}
				minusMoney(getClangMoney()); // 돈 감소
				plusHappy(5); //행복도 5증가
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
		}
	}	
	
	
	
	
	public void cpp() {
		
		String subjectName = "C+";
		
		if (getMoney() < getCppMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
				int level = getCpp();
				level += PointReturn_Hard(); // 15점 증가
				int plusPoint = getCpp() - level;
				setCpp(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				//_SetPrint.MSG(getCpp() + ")", 600);
				itemCheck();
				if (getCpp() >= 200 || getCshop() >= 200) {
					level = getCpp();
					level += 30; // 15점 증가
					setCpp(level);
					//_SetPrint.MSG("C+ or C# 능력으로 30점 추가!",600);
				} else if(getClang() >= 30 || getCshop() >= 30) { //30점 이상이면서 120이하
					if(getClang() <= 120 || getCshop() <= 120) {
						MinusCseris(); //비슷한 용어능력치 감소
					}
				}
				
				minusMoney(getCppMoney()); // 돈 감소
				plusHappy(6); //행복도 5증가
				
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
		}
	}	
	
	
	public void cshop() {
		
		String subjectName = "C#";
		
		if (getMoney() < getCshopMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
				int level = getCshop();
				level += PointReturn_Hard(); // 15점 증가
				int plusPoint = getCshop() - level;
				setCshop(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				//_SetPrint.MSG(getCshop() + ")", 600);
				itemCheck();
				if (getCshop() >= 200 || getCshop() >= 200) {
					level = getCshop();
					level += 30; // 15점 증가
					setCshop(level);
					//_SetPrint.MSG("C+ or C# 능력으로 30점 추가!",600);
				} else if(getClang() >= 30 || getCpp() >= 30) { //30점 이상이면서 120이하
					if(getClang() <= 120 || getCpp() <= 120) {
						MinusCseris(); //비슷한 용어능력치 감소
					}
				}
				
				minusMoney(getCshopMoney()); // 돈 감소
				plusHappy(6); //행복도 5증가
				
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
		}
	}	
	
	
	public void python() {
		
		String subjectName = "Python";
		
		if (getMoney() < getPythonMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
				int level = getPython();
				level += PointReturn_Midium(); // 15점 증가
				int plusPoint = getPython() - level;
				setPython(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				//_SetPrint.MSG(getPython() + ")", 600);
				itemCheck();
				if (getClang() >= 200) {
					level = getPython();
					level += 15; // 15점 증가
					setPython(level);
					//_SetPrint.MSG("C+ 능력으로 15점 추가!",600);
				} else if(getClang() >= 30) { //30점 이상이면서 120이하
					if(getClang() <= 120) {
						MinusClang(); //비슷한 용어능력치 감소
					}
				}
				
				minusMoney(getPythonMoney()); // 돈 감소
				plusHappy(6); //행복도 5증가
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
		}
	}
	
	
	public void objective() {
		
		String subjectName = "objective-c";
		
		if (getMoney() < getObject_cMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
				int level = getObject_c();
				level += PointReturn_Hard(); // 15점 증가
				int plusPoint = getObject_c() - level;
				setObject_c(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				//_SetPrint.MSG(getObject_c() + ")", 600);
				itemCheck();
				if (getSwift() >= 200) {
					level = getObject_c();
					level += 20; 
					setObject_c(level);
					//_SetPrint.MSG("Swift 능력으로 15점 추가!",600);
				} else if(getSwift() >= 30) { //30점 이상이면서 120이하
					if(getSwift() <= 120) {
						MinusApple(); //비슷한 용어능력치 감소
					}
				}
				
				minusMoney(getObject_cMoney()); // 돈 감소
				plusHappy(8); //행복도 5증가
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
		}
	}
	
	
	public void swift() {
		
		String subjectName = "Swift";
		
		if (getMoney() < getSwiftMoney()) { 
			noMoney(subjectName); // 돈 부족 
		} else {
				int level = getSwift();
				level += PointReturn_Hard(); // 15점 증가
				int plusPoint = getSwift() - level;
				setSwift(level);
				
				PrintMsg(subjectName, plusPoint); //능력치 이름
				//_SetPrint.MSG(getSwift() + ")", 600);
				itemCheck();
				if (getObject_c() >= 200) {
					level = getSwift();
					level += 20; 
					setSwift(level);
					//_SetPrint.MSG("Objective-C 능력으로 20점 추가!",600);
				} else if(getObject_c() >= 30) { //30점 이상이면서 120이하
					if(getObject_c() <= 120) {
						MinusApple(); //비슷한 용어능력치 감소
					}
				}
				
				minusMoney(getSwiftMoney()); // 돈 감소
				plusHappy(8); //행복도 5증가
				minusDay(); // 일정 횟수 감소
				//returnMenu(); // 메뉴로 돌아가자
		}
	}
	
	public void vacation() {
		_SetPrint.Studing("휴식중", 600);
		_SetPrint.MSG("휴식은 정말 좋아!", 600);
		
		startpoint = 7;
		endpoint = 42;
		rangepoint = endpoint - startpoint + 1;
		int randomInt7to42 = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		
		plusHappy(randomInt7to42); //행복도  7~42증가
		minusDay(); // 일정 횟수 감소
		
		//모든 능력치 감소
		startpoint = 3;
		endpoint = 5;
		rangepoint = endpoint - startpoint + 1;
		int randomIntMinus = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		am.AllminusData(randomIntMinus); 
		_SetPrint.MSGL("모든 능력치" + randomIntMinus + "만큼 감소했습니다.", 600);
		itemCheck();
		int minusData = getAlba();
		minusData -= randomIntMinus;
		setAlba(minusData);
		
		//returnMenu(); // 메뉴로 돌아가자
	}
	
	
	
	public void alba() {
		
		if (getWorkTime() == 4) {
			String subjectName = "아르바이트";
	
			int level = getAlba();
			level += PointReturn_Easy(); // 30점 증가
			int plusPoint = getAlba() - level;
			setAlba(level);
			
			//모든 능력치 감소
			startpoint = 13;
			endpoint = 21;
			rangepoint = endpoint - startpoint + 1;
			int randomIntMinus = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);

			itemCheck();
			PrintAlba(subjectName, plusPoint, randomIntMinus);
			_SetPrint.MSG(getAlba() + ")", 650);
			
			startpoint = 250;
			endpoint = 880;
			rangepoint = endpoint - startpoint + 1;
			int randomInt280to880 = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
			plusMoney(randomInt280to880); // 돈 증가
			
			minusDay(); // 일정 횟수 감소
			minusDay(); 
			minusDay(); 
			minusDay(); 
		
			
			
			startpoint = 22;
			endpoint = 42;
			rangepoint = endpoint - startpoint + 1;
			int randomInt22to39 = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);

			minusHappy(randomInt22to39); // 행복도 22~39감소

			_SetPrint.MSG("알바를 제외한 모든 능력치 " +randomIntMinus+ "만큼 감소했습니다.", 600);
			
			setAlbaCheck(1); //알바엔딩
			
			//returnMenu(); // 메뉴로 돌아가지
		} else {
			_SetPrint.MSG("오늘은 시간이 안되서 알바를 할 수 없어", 800);
			//returnMenu(); 
		}
	}
	
	
	
	public void goOut() {
		RandomEvent randEv = new RandomEvent();
		startpoint = 1;
		endpoint = 88;
		rangepoint = endpoint - startpoint + 1;
		int randGoOut = (int)(randomGenerator.nextDouble() * rangepoint + startpoint);
		
		if (randGoOut==3 || randGoOut==6 ||randGoOut==12 || randGoOut==22 || randGoOut==31 || randGoOut==52 ||randGoOut==77 || randGoOut==82) {
			randEv.MoneyUp();
		} else if (randGoOut==4 || randGoOut==7 ||randGoOut==13 || randGoOut==23 || randGoOut==32 || randGoOut==53 ||randGoOut==76 || randGoOut==83) {
			randEv.MoneyDown();
		} else if (randGoOut==5 || randGoOut==8 ||randGoOut==14 || randGoOut==24 || randGoOut==33 || randGoOut==54 || randGoOut==65 ||randGoOut==79) {
			randEv.LoveStory();
		} else if (randGoOut==11 || randGoOut==16 || randGoOut==32 || randGoOut==45 ||randGoOut==62) {
			randEv.OldFriendsMeet();
		} else if (randGoOut==15 || randGoOut==33 || randGoOut==46 ||randGoOut==63) {
			randEv.OldFriendsMeet2();
		} else if (randGoOut==16 || randGoOut==34 || randGoOut==47 ||randGoOut==64) {
			randEv.OldFriendsMeet3();
		} else if (randGoOut==17 || randGoOut==35 || randGoOut==48 ||randGoOut==65) {
			randEv.OldFriendsMeet4();
		} else if (randGoOut==19 || randGoOut==25 || randGoOut==35 ||randGoOut==41 ||randGoOut==53 ||randGoOut==68 ||randGoOut==72) {
			randEv.Crischan();
		} else if (randGoOut==1) {
			randEv.HtmlMaster();
		} else if (randGoOut==2) {
			randEv.JqueryMaster();
		} else if (randGoOut==3) {
			randEv.JavaMaster();
		} else if (randGoOut==9 || randGoOut==80 || randGoOut==66) {
			randEv.BestAlba();
		} else {
			randEv.BasicEv();
		}
		
		
		minusDay();
		
		int gpoint = getGoOutPoint();
		gpoint++;
		setGoOutPoint(gpoint);
		
		//returnMenu(); // 메뉴로 돌아가지
	}
	
	

}
