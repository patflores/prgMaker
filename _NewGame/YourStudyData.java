﻿package _NewGame;

public class YourStudyData extends YourData {

	
	public void ShowList() {
		_SetPrint.MSG("\n", 400);
		_SetPrint.MSG("=====현재 내 정보=====", 400);
		_SetPrint.MSG("이름 : " + getName(), 400);
		_SetPrint.MSG("나이 : " + getAge(), 400);
		_SetPrint.MSG("성별 : " + getJender(), 400);
		_SetPrint.MSG("자금 : " + getMoney(), 400);
		_SetPrint.MSG("기분 : " + getHappy(), 400);
		_SetPrint.MSG("=====현재 내 능력치=====", 400);
		_SetPrint.MSG("HTML/CSS : " + getHtml(), 400);
		_SetPrint.MSG("JavaScript/Jquery : " + getJquery(), 400);
		_SetPrint.MSG("PHP : " + getPhp(), 400);
		_SetPrint.MSG("ASP : " + getAsp(), 400);
		_SetPrint.MSG("JAVA : " + getJava(), 400);
		_SetPrint.MSG("JSP : " + getJsp(), 400);
		_SetPrint.MSG("C : " + getClang(), 400);
		_SetPrint.MSG("C++ : " + getCpp(), 400);
		_SetPrint.MSG("C# : " + getCshop(), 400);
		_SetPrint.MSG("Python : " + getPython(), 400);
		_SetPrint.MSG("Objective-C : " + getObject_c(), 400);
		_SetPrint.MSG("Swift : " + getSwift(), 400);
		_SetPrint.MSG("알바능력 : " + getAlba(), 400);
	}
	
	public void ShowData() {
		ShowList();
		int continueChk = 0;
		_SetPrint.MSG("\n이전메뉴로 돌아갈까?",600);	
		_SetPrint.MSG("아무키나 누르면 이전메뉴로 돌아갑니다", 600);
		
		SelectWorkMenu sw = new SelectWorkMenu();
		try {
			continueChk = Integer.parseInt(ReadValue.input());
		} catch (NumberFormatException e) {
			sw.menuSelect();
		} catch (Exception e) {}
		
		sw.menuSelect();
	}
	
	

}

